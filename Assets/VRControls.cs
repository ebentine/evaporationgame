﻿using UnityEngine;

public class VRControls : MonoBehaviour {

	void Start () {
        _integrator = FindObjectOfType<AtomCloudTimeIntegrator>();
	}

    public void Update()
    {
        // If button is pressed, reset the simulation. Max once every 2s.
        if (IsResetPressed() && _resetTimer <= 0f)
        {
            _resetTimer = 2f;
            _integrator.Reset();
        }
        else
            _resetTimer -= Time.deltaTime;
    }

    /// <summary>
    /// Check if the reset button is pressed
    /// </summary>
    /// <returns></returns>
    public bool IsResetPressed()
    {
        var device = GetController();
        if (device != null && device.GetPress(SteamVR_Controller.ButtonMask.Grip))
        {
            device.TriggerHapticPulse(5000);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Get a controller
    /// </summary>
    /// <returns></returns>
    public SteamVR_Controller.Device GetController()
    {
        var index = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.First, Valve.VR.ETrackedDeviceClass.Controller);
        if (index == (int)SteamVR_TrackedObject.EIndex.None)
            return null;
        return SteamVR_Controller.Input(index);
    }

    private float _resetTimer = 0f;
    private AtomCloudTimeIntegrator _integrator;
}
