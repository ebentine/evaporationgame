﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

/// <summary>
/// Integrates the equation of motion for atoms, performs propagation, applies forces, etc.
/// </summary>
public class AtomCloudTimeIntegrator : MonoBehaviour
{
    /// <summary>
    /// Holds a list of all spawned objects.
    /// </summary>
    //private List<GameObject> m_spawned = new List<GameObject>();

    #region AtomSpawners

    public List<IAtom> Atoms { get; protected set; }

    protected List<AtomCloud> m_atomClouds;

    public void AddAtom(IAtom atom)
    {
        Atoms.Add(atom);

        byte b = (byte)atom.AtomTypeGroup;
        if (m_atomsByType.ContainsKey(b))
            m_atomsByType[b].Add(atom);
        else
        {
            m_atomsByType.Add(b, new List<IAtom>());
            m_atomsByType[b].Add(atom);
        }
    }

    public void RemoveAtom(IAtom atom)
    {
        Atoms.Remove(atom);

        byte b = (byte)atom.AtomTypeGroup;
        if (m_atomsByType.ContainsKey(b))
        {
            m_atomsByType[b].Remove(atom);
            if (m_atomsByType[b].Count == 0)
                m_atomsByType.Remove(b);
        }
    }

    private Dictionary<byte, List<IAtom>> m_atomsByType;

    public List<AtomBehaviour.AtomGroup> Groups()
    {
        //return new List<AtomBehaviour.AtomGroup>(m_atomsByType.Keys);
        List<AtomBehaviour.AtomGroup> groups = new List<AtomBehaviour.AtomGroup>();
        foreach (byte b in m_atomsByType.Keys)
        {
            groups.Add((AtomBehaviour.AtomGroup)b);
        }
        return groups;
    }

    /// <summary>
    /// Get a list of atoms corresponding to given atom type.
    /// </summary>
    /// <param name="group"></param>
    /// <returns></returns>
    public List<IAtom> GetAtomsByGroup(AtomBehaviour.AtomGroup group)
    {
        if (m_atomsByType.ContainsKey((byte)group))
            return m_atomsByType[(byte)group];
        return new List<IAtom>();
    }

    #endregion

    #region Modifiers

    /// <summary>
    /// List of all AtomModifiers in the scene that influence the atoms
    /// </summary>
    protected List<AtomModifier> m_modifierList;

    #endregion

    #region Collisions

    /// <summary>
    /// Should the cloud calculate collisions between atoms?
    /// </summary>
    public bool DoCollisions = true;

    /// <summary>
    /// Simulation grid used to calculate collisions
    /// </summary>
    MonteCarloCollisionGrid m_collisionGrid;

    /// <summary>
    /// Size of the Monte-Carlo Collision grid
    /// </summary>
    public float CollisionGridSize = 10f;

    /// <summary>
    /// Number of cells wide, high, deep for Monte-Carlo collision grid
    /// </summary>
    public int CollisionGridCellSize = 40;

    #endregion

    #region Debugging

    /// <summary>
    /// If true, draws the MonteCarloCollisionGrid each rendering cycle.
    /// </summary>
    public bool DebugDrawCollisionGrid;

    #endregion

    public void Start()
    {
        Atoms = new List<IAtom>();
        m_collisionGrid = new MonteCarloCollisionGrid(new Vector3(CollisionGridSize, CollisionGridSize, CollisionGridSize), CollisionGridCellSize, CollisionGridCellSize, CollisionGridCellSize);

        m_atomsByType = new Dictionary<byte, List<IAtom>>();

        //Identify AtomModifiers in the scene
        m_modifierList = new List<AtomModifier>();
        m_modifierList.AddRange(FindObjectsOfType<AtomModifier>());

        //Identify AtomClouds in the scene
        m_atomClouds = new List<AtomCloud>();
        m_atomClouds.AddRange(FindObjectsOfType<AtomCloud>());

        //spawn atom clouds and add atoms to global list.
        m_atomClouds.ForEach(ac => ac.Spawn().ForEach(a => AddAtom(a)));
    }

    /// <summary>
    /// Update the positions of all atoms in this atom cloud
    /// </summary>
    public void FixedUpdate()
    {
        //Propagate position of atoms
        Atoms.ForEach(a => a.Propagate());

        //Scene-wide atom modifiers change properties of atoms
        m_modifierList.ForEach(m => Atoms.ForEach(a => m.TryModify(a)));

        //Perform collisions between pairs of atoms
        if (DoCollisions)
            m_collisionGrid.DoCollisions(Atoms);
    }

    /// <summary>
    /// Reset the simulation
    /// </summary>
    public void Reset()
    {
        m_atomClouds.ForEach(ac => ac.Reset());
        foreach (var chart in FindObjectsOfType<MeasurableGraph>())
            chart.Reset();

        foreach (var trans in FindObjectsOfType<BECTransition>())
        {
            trans.Reset();
        }
    }

    void Update()
    {
        //If someone pressed reset button, reset.
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //reset rf knife.
            RFKnife knife = FindObjectOfType<RFKnife>();
            if (knife != null)
                knife.Reset();

            //reset charts
            foreach (var chart in FindObjectsOfType<MeasurableGraph>())
                chart.Reset();

            Reset();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        if (DebugDrawCollisionGrid)
            m_collisionGrid.DrawDebug();
    }

}
