﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a natural 1/e decay of atoms in the trap
/// </summary>
public class BackgroundGas : MonoBehaviour
{
    /// <summary>
    /// Should the temperature of an atom be reset on collision with the boundary?
    /// </summary>
    [Tooltip("Flux of background gas into the enclosed boundary")]
    public float SpawnRate;

    [Tooltip("Max velocity of spawned background particles.")]
    public float Velocity;

    [Tooltip("Template used for atom spawning.")]
    public GameObject Template;

    /// <summary>
    /// Extent of the periodic boundary
    /// </summary>
    [Tooltip("Extent of the periodic boundary")]
    public Vector3 Extent;

    public AtomCloudTimeIntegrator Integrator { get; private set; }

    public void Start()
    {
        //Identify the Integrator in the scene.
        Integrator = FindObjectOfType<AtomCloudTimeIntegrator>();

        if (Integrator == null)
            Debug.LogError("Could not find integrator on level.");

        m_spawned = new List<IAtom>();
    }

    private List<IAtom> m_spawned;

    private float m_timeSinceLastSpawn = 0f;

    public void FixedUpdate()
    {
        //Remove any atoms that we have created that have escaped the volume.
        List<IAtom> spawned = new List<IAtom>(m_spawned);
        foreach (IAtom atom in spawned)
        {
            if (IsAtomOutside(atom))
            {
                m_spawned.Remove(atom);
                Integrator.RemoveAtom(atom);
                atom.Destroy();
            }
        }

        m_timeSinceLastSpawn -= Time.fixedDeltaTime;
        while (m_timeSinceLastSpawn < 0f)
        {
            Vector3 p = GetRandomPositionOnWall();
            Vector3 v = Random.insideUnitSphere;
            if (Vector3.Dot(p, v) > 0f)
                v -= 2 * p * Vector3.Dot(p, v);
            v /= Velocity * v.magnitude;

            CreateAtom(p, v);

            //Debug.DrawLine(p, Vector3.zero, Color.black, float.PositiveInfinity);
            m_timeSinceLastSpawn += 1/SpawnRate;
        }
    }

    public bool IsAtomOutside(IAtom atom)
    {
        Vector3 pos = atom.Position + Extent / 2f;
        return (pos.x < 0 || pos.y < 0 || pos.z < 0 || pos.x > Extent.x || pos.y > Extent.y || pos.z > Extent.z);
    }

    /// <summary>
    /// Get a random position on the boundary sides
    /// </summary>
    /// <returns></returns>
    public Vector3 GetRandomPositionOnWall()
    {
        //Determine position to spawn;
        Vector3 spawnPos = new Vector3(Random.value * Extent.x, Random.value * Extent.y) - Extent / 2f;

        //Determine which side to spawn on
        float a = Random.value; Vector3 aim;
        if (6*a < 1f)
            aim = Vector3.up;
        else if (6*a < 2f)
            aim = Vector3.down;
        else if (6*a < 3f)
            aim = Vector3.right;
        else if (6*a < 4f)
            aim = Vector3.left;
        else if (6*a < 5f)
            aim = Vector3.forward;
        else
            aim = Vector3.back;

        Quaternion q = Quaternion.FromToRotation(Vector3.back, aim);
        return q * spawnPos;
    }

    public void CreateAtom(Vector3 position, Vector3 velocity)
    {
        GameObject obj = GameObject.Instantiate(Template);
        IAtom atom = obj.GetComponent<IAtom>();
        if (atom != null)
        {
            m_spawned.Add(atom);
            Integrator.AddAtom(atom);
            atom.Position = position;
            atom.Velocity = velocity;
            atom.Reset();
            atom.Propagate();
        }
        else
        {
            Debug.Log("Could not create atom - specified template does not contain IAtom behaviour.");
            obj.SetActive(false);
        }
    }
}