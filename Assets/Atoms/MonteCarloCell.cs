﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A cell used in the Monte-Carlo simulation
/// </summary>
public class MonteCarloCell {

    /// <summary>
    /// List of atoms within this Monte-Carlo simulation cell
    /// </summary>
    protected List<IAtom> m_atoms;

    /// <summary>
    /// Size of the simulation cell
    /// </summary>
    public Vector3 Extent { get; protected set; }

    /// <summary>
    /// Create a new Monte-Carlo cell
    /// </summary>
    /// <param name="extent"></param>
    public MonteCarloCell(Vector3 extent)
    {
        m_atoms = new List<IAtom>();
        Extent = extent;
        Volume = Extent.x * Extent.y * Extent.z;
    }

    public void DoCollisions()
    {
        //Create a list of all atomic pairs
        for (int i=0;i<m_atoms.Count;i++)
            for (int j = 0; j < i; j++)
            {
                float chance = DetermineCollisionChance(m_atoms[i], m_atoms[j]);
                if (Random.value < chance)
                    m_atoms[i].Collide(m_atoms[j]);
            }

    }

    /// <summary>
    /// Determine the chance of collisions occuring between two atoms in the cell
    /// </summary>
    /// <returns></returns>
    public float DetermineCollisionChance(IAtom atom1, IAtom atom2)
    {
        //The method used is this: 
        // 1. Determine 2*mean atomic radius
        // 2. Determine the volume of the simulation cell
        // 3. Chance of collision = 2* mean atomic volume / simulation cell volume

        if (!atom1.IsTrapped || !atom2.IsTrapped || !atom1.CanCollide || !atom2.CanCollide)
            return 0f;

        float meanR = (atom1.Radius + atom2.Radius);
        float vol = 4 * Mathf.PI / 3 * Mathf.Pow(meanR,3);

        return vol / Volume;
    }

    public float Volume { get; private set; }

    public void AddAtom(IAtom atom)
    {
        m_atoms.Add(atom);
    }

    public void Clear()
    {
        m_atoms.Clear();
    }

}
