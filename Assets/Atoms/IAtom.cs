﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An atom in the simulation
/// </summary>
public interface IAtom {

    /// <summary>
    /// Velocity of the atom
    /// </summary>
    Vector3 Velocity { get; set; }

    /// <summary>
    /// Radius of the atom
    /// </summary>
    float Radius { get; }

    /// <summary>
    /// Collide with a second atom
    /// </summary>
    /// <param name="atom"></param>
    void Collide(IAtom atom);

    /// <summary>
    /// Another atom collided into this one
    /// </summary>
    /// <param name="partner"></param>
    void OnCollided(IAtom partner);

    /// <summary>
    /// Propagate the atom in space
    /// </summary>
    void Propagate();

    /// <summary>
    /// Reset the atom to default values
    /// </summary>
    void Reset();

    Vector3 Position { get; set; }

    bool IsTrapped { get; set; }

    /// <summary>
    /// Can this atom collide with another?
    /// </summary>
    bool CanCollide { get; set; }

    /// <summary>
    /// Group this atom belongs to.
    /// </summary>
    AtomBehaviour.AtomGroup AtomTypeGroup { get; set; }

    void Destroy();

    /// <summary>
    /// The mass of this atom
    /// </summary>
    float Mass { get; }
}
