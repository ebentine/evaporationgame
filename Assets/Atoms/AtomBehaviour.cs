﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomBehaviour : MonoBehaviour, IAtom
{
    public bool DrawDebug;

    // Use this for initialization
    void Start()
    {
        CanCollide = true;
        Reset();
    }

    #region Atom properties

    public enum AtomGroup : byte
    {
        A = 1 << 0,
        B = 1 << 1,
        C = 1 << 2,
        D = 1 << 3,
        E = 1 << 4,
        F = 1 << 5,
        G = 1 << 6,
        H = 1 << 7,
    }

    /// <summary>
    /// Atom type.
    /// </summary>
    public AtomGroup Group;

    public AtomGroup AtomTypeGroup { get { return Group; } set { Group = value; } }

    /// <summary>
    /// Position of the atom in 3D space
    /// </summary>
    public Vector3 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    /// <summary>
    /// Linear velocity of the atom
    /// </summary>
    public Vector3 Velocity { get; set; }

    public float CollisionRadius = 0.15f;
    public float Radius { get { return CollisionRadius; } }

    /// <summary>
    /// Can this atom collide with another?
    /// </summary>
    public bool CanCollide { get; set; }

    public float Mass { get { return 1f; } }

    #endregion

    #region Rendering

    private MaterialPropertyBlock m_propertyBlock;
    private Renderer m_renderer;

    #endregion

    public void Destroy()
    {
        GameObject.Destroy(gameObject);
    }

    Color Color;

    public void Awake()
    {
        m_propertyBlock = new MaterialPropertyBlock();
        m_renderer = GetComponent<Renderer>();
    }

    /// <summary>
    /// Is the atom currently trapped in the potential.
    /// </summary>
    public bool IsTrapped
    {
        get { return m_isTrapped; }
        set
        {
            m_isTrapped = value;
            Color = UntrappedColor;
        }
    }
    protected bool m_isTrapped;

    /// <summary>
    /// Resets the atom to trapped state with initial velocity.
    /// </summary>
    public void Reset()
    {
        gameObject.SetActive(true);
        IsTrapped = true;
        Color = NaturalColor();
    }

    public void Propagate()
    {
        if (DrawDebug)
            Debug.DrawLine(transform.position, transform.position + Velocity * Time.fixedDeltaTime, Color.red);

        // update position based on velocity
        transform.position = transform.position + Velocity * Time.fixedDeltaTime;

        //Disable atom if it leaves the simulation region
        if (transform.position.y < -100.0f)
            gameObject.SetActive(false);
    }

    public void Collide(IAtom other)
    {
        // Velocity in center of mass frame
        Vector3 comv = (Velocity + other.Velocity) / 2f;

        //transform velocities to CoM frame
        Vector3 a = Velocity - comv;
        Vector3 b = other.Velocity - comv;

        // swap velocities in CoM frame
        Velocity = comv + b;
        other.Velocity = comv + a;

        //Change both atoms color to red.
        Color = CollisionColor;
        OnCollided(other);
        other.OnCollided(this);
    }

    public virtual void OnCollided(IAtom partner)
    {
        Color = CollisionColor;
    }

    /// <summary>
    /// Color of the atom when not collided
    /// </summary>
    public Color NaturalColor()
    {
        if (IsTrapped) return TrappedColor;
        return UntrappedColor;
    }

    public Color TrappedColor;

    public Color UntrappedColor;

    /// <summary>
    /// Color of the atom immediately after a collision
    /// </summary>
    public Color CollisionColor;



    public void Update()
    {
        Color = Color.Lerp(Color, NaturalColor(), Time.deltaTime / 0.1f);

        m_renderer.GetPropertyBlock(m_propertyBlock);
        m_propertyBlock.SetColor("_Color", Color);
        m_renderer.SetPropertyBlock(m_propertyBlock);
    }
}
