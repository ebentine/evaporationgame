﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomCloud : MonoBehaviour {

    /// <summary>
    /// Number of atoms to spawn.
    /// </summary>
    public int NumberToSpawn = 200;

    /// <summary>
    /// Radius within which to spawn atoms.
    /// </summary>
    public float SpawnRadius = 2.0f;

    public float InitialVelocity = 3.0f;

    /// <summary>
    /// Template to spawn.
    /// </summary>
    public GameObject template;

    /// <summary>
    /// Holds a list of all spawned objects.
    /// </summary>
    private List<GameObject> m_spawned = new List<GameObject>();

    public List<IAtom> Atoms { get; protected set; }

    /// <summary>
    /// Create a number of atoms from template and initialise positions and velocities.
    /// </summary>
    /// <returns></returns>
    public List<IAtom> Spawn(int number)
    {
        Atoms = new List<IAtom>();

        if (!enabled)
            return Atoms;

        //Populate object pool with generated atoms.
        for (int i = 0; i < number; i++)
        {
            GameObject obj = GameObject.Instantiate(template);
            m_spawned.Add(obj);
            IAtom atom = obj.GetComponent<IAtom>();
            if (atom != null)
            {
                Atoms.Add(atom);
            }
            else
            {
                Debug.Log("Could not create atom - specified template does not contain IAtom behaviour.");
                obj.SetActive(false);
                return Atoms;
            }
        }

        Reset();
        return Atoms;
    }

    /// <summary>
    /// Create configured number of atoms from template and initialise positions and velocities.
    /// </summary>
    /// <returns></returns>
    public List<IAtom> Spawn()
    {
        return Spawn(NumberToSpawn);
    }

    /// <summary>
    /// Resets the atom cloud spawner, generating a new cloud of atoms.
    /// </summary>
    public void Reset()
    {
        foreach (IAtom atom in Atoms)
        {
            atom.Velocity = Random.insideUnitSphere * InitialVelocity;
            atom.Position = Random.insideUnitSphere * SpawnRadius;
            atom.Reset();
        }
    }

    void Update()
    {
        //If someone pressed reset button, reset.
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //reset rf knife.
            RFKnife knife = FindObjectOfType<RFKnife>();
            if (knife != null)
                knife.Reset();

            Reset();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #else
            Application.Quit();
            #endif
        }
    }

}
