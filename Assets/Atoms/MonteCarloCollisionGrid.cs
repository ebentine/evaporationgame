﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Performs collisions of atoms using a Monte-Carlo approach.
/// The grid is centered on the origin with Extent in x,y,z
/// </summary>
public class MonteCarloCollisionGrid {

    public MonteCarloCollisionGrid(Vector3 Extent, int width, int height, int depth)
    {
        this.Extent = Extent;
        Width = width;
        Height = height;
        Depth = depth;

        float cw = Extent.x / width;
        float cy = Extent.y / height;
        float cz = Extent.z / depth;

        CellExtent = new Vector3(cw, cy, cz);

        //Initialise cell array
        m_cells = new MonteCarloCell[Width * Height * Depth];
        AllCells = new List<MonteCarloCell>();
        for (int i = 0; i < Width*Height*Depth; i++)
        {
            m_cells[i] = new MonteCarloCell(CellExtent);
            AllCells.Add(m_cells[i]);
        }
    }

    protected MonteCarloCell[] m_cells;


    protected List<MonteCarloCell> AllCells;

    /// <summary>
    /// Width of the Monte Carlo grid in cells
    /// </summary>
    public int Width { get; protected set; }

    /// <summary>
    /// Height of the Monte Carlo grid in cells
    /// </summary>
    public int Height { get; protected set; }

    /// <summary>
    /// Depth of the Monte Carlo grid in cells
    /// </summary>
    public int Depth { get; protected set; }

    /// <summary>
    /// Physics size of a cell
    /// </summary>
    public Vector3 CellExtent { get; protected set; }

    public Vector3 Extent { get; protected set; }

    /// <summary>
    /// Gets cell associated with given x,y,z position in space, or null if one does not exist.
    /// </summary>
    public MonteCarloCell GetCell(Vector3 position)
    {
        // First shift the coordinates so that the position (0,0,0) corresponds to the edge vertex of the simulation region
        position = position + Extent / 2f;

        // Get x,y,z of cells
        int x = (int)(position.x / CellExtent.x);
        int y = (int)(position.y / CellExtent.y);
        int z = (int)(position.z / CellExtent.z);

        if (x < 0 || y < 0 || z < 0 || x >= Width || y >= Height || z >= Depth)
            return null;

        int index = x + y * Width + z * Width * Height;
        return m_cells[index];
    }

    /// <summary>
    /// Draw collision grid for debug visualisation
    /// </summary>
    public void DrawDebug()
    {
        Vector3 u = Vector3.right * CellExtent.x;
        Vector3 v = Vector3.up * CellExtent.y;
        Vector3 w = Vector3.forward * CellExtent.z;
        for (int x = 0; x < Width; x++)
            for (int y = 0; y < Width; y++)
                for (int z = 0; z < Height; z++)
                {
                    Vector3 gridOff = -Extent / 2f;
                    Vector3 pos = new Vector3(CellExtent.x * x, CellExtent.y * y, CellExtent.z * z);
                    pos = pos + gridOff;

                    // Draw debug lines.
                    //bottom slab
                    Debug.DrawLine(pos, pos + u, Color.black);
                    Debug.DrawLine(pos, pos + w, Color.black);
                    Debug.DrawLine(pos + u, pos + u + w, Color.black);
                    Debug.DrawLine(pos + w, pos + u + w, Color.black);
                    
                    //top slab
                    Debug.DrawLine(pos + v, pos + u + v, Color.black);
                    Debug.DrawLine(pos + v, pos + w + v, Color.black);
                    Debug.DrawLine(pos + u + v, pos + u + w + v, Color.black);
                    Debug.DrawLine(pos + w + v, pos + u + w + v, Color.black);

                    //pillars
                    Debug.DrawLine(pos, pos + v, Color.black);
                    Debug.DrawLine(pos + u, pos + u + v, Color.black);
                    Debug.DrawLine(pos + w, pos + w + v, Color.black);
                    Debug.DrawLine(pos + w + v, pos + u + w + v, Color.black);
                }
    }

    public void DoCollisions(List<IAtom> atoms)
    {
        // Start by emptying all cells
        AllCells.ForEach(p => p.Clear());

        //Sow atoms among cells
        MonteCarloCell cell;
        foreach (IAtom atom in atoms)
        {
            cell = GetCell(atom.Position);
            if (cell != null)
                cell.AddAtom(atom);
        }

        //Do collisions for each cell
        AllCells.ForEach(c => c.DoCollisions());
    }
}
