using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PSDensityMeasurement : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	public void Reset()
	{
		Renderer render = GetComponent<Renderer>();
		if (render != null)
			render.material.color = Color.yellow;
		//render.sharedMaterial.color = Color.white;
	}

	public int MinCount = 10;
	public float Density;
	public float PSDensity;
	public float Temperature;
	float lastUpdate = -100f;


	// Update is called once per frame

	void Update ()
	{
		if (Time.time < lastUpdate + 0.5f)
			return;

		lastUpdate = Time.time;

		//Get all atoms
		int numberAtoms = 0;
		float energy = 0.0f;
		foreach (AtomBehaviour a in FindObjectsOfType<AtomBehaviour>())
		{
			if (!a.IsTrapped)
				continue;
			energy += a.Velocity.sqrMagnitude;
			numberAtoms++;
		}

		//Get trap radius
		float trapRadius = 0.0f;
		foreach (RFKnife trap in FindObjectsOfType<RFKnife>())
		{
			trapRadius = trap.KnifeRadius;
		}

		//Calculate density and phase-space density
		float Density = numberAtoms / Mathf.Pow(trapRadius, 2f);
		if (numberAtoms > MinCount)
			Temperature = energy / numberAtoms;
		float PSDensity = Density / Mathf.Pow(Temperature, 1.5f);

		TextMesh tm = GetComponent<TextMesh>();
		if (tm != null)
		{
			tm.text = "PSD= " + PSDensity.ToString("000.0\n") +
				"T= " + Temperature.ToString("00.0\n") +
				"R= " + trapRadius.ToString("0.0\n") +
				"rho= " + Density.ToString("0.0\n") +
				"N= " + numberAtoms.ToString("0") ;
			if (numberAtoms < MinCount)
			{
				Renderer render = GetComponent<Renderer>();
				if (render != null)
					render.material.color = Color.gray;
				//render.sharedMaterial.color = Color.gray;
			}
			else
			{
				if (PSDensity > 2.7f) // critical density for BEC
				{
					Renderer render = GetComponent<Renderer>();
					if (render != null)
						render.material.color = Color.red;
					//render.sharedMaterial.color = Color.gray;
				}
			}
		}
	}
}
