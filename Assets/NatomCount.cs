﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NatomCount : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	public void Reset()
	{
		Renderer render = GetComponent<Renderer>();
		if (render != null)
			render.material.color = Color.yellow;
	}

	public int MinCount = 10;
	public int numberAtoms ;
	float lastUpdate = -100f;


	// Update is called once per frame

	void Update ()
	  {
		if (Time.time < lastUpdate + 0.5f)
			return;

		lastUpdate = Time.time;

		//Get all atoms
		numberAtoms = 0;
		foreach (AtomBehaviour a in FindObjectsOfType<AtomBehaviour>())
		  {
			if (!a.IsTrapped)
				continue;
			numberAtoms++;
		  }
		TextMesh tm = GetComponent<TextMesh>();
		if (tm != null)
		{
			tm.text = "N= " + numberAtoms.ToString("0");
			if (numberAtoms < MinCount)
			{
				Renderer render = GetComponent<Renderer>();
				if (render != null)
					render.material.color = Color.gray;
			}
		}
	  }
}
