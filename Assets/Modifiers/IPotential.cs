﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Affects the potential energy of an atom.
/// </summary>
public interface IPotential
{
    /// <summary>
    /// Calculate the potential energy of an atom affected by this IPotential.
    /// </summary>
    float CalculatePotentialEnergy(IAtom atom);

    int DegreesOfFreedom { get; }
}