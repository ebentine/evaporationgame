﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a natural 1/e decay of atoms in the trap
/// </summary>
public class PeriodicBoundaryCondition : AtomModifier
{
    /// <summary>
    /// Should the temperature of an atom be reset on collision with the boundary?
    /// </summary>
    [Tooltip("Should the temperature of an atom be reset on collision with the boundary?")]
    public bool ResetTemperatureOnCollision;

    public float Temperature;

    /// <summary>
    /// Extent of the periodic boundary
    /// </summary>
    [Tooltip("Extent of the periodic boundary")]
    public Vector3 Extent;

    public override void Modify(IAtom atom)
    {
        if (IsAtomOutside(atom))
        {
            Vector3 velocity = atom.Velocity;
            if (ResetTemperatureOnCollision)
            {
                velocity = Mathf.Pow(Temperature, 0.5f) * Random.insideUnitSphere;
            }

            //Pick a wall on which atom will reappear
            // TODO

        }
    }

    public bool IsAtomOutside(IAtom atom)
    {
        Vector3 offset = transform.position - Extent / 2f;
        Vector3 pos = atom.Position - offset;
        return (pos.x < 0 || pos.y < 0 || pos.z < 0 || pos.x > Extent.x || pos.y > Extent.y || pos.z > Extent.z);
    }
}