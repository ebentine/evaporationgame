﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


/// <summary>
/// Represents an object that can modify an atom
/// </summary>
public class AtomModifier : MonoBehaviour
{
    public virtual void Modify(IAtom atom) { }

    /// <summary>
    /// Tries to modify the atom, fails if !AffectAtom.
    /// </summary>
    /// <param name="atom"></param>
    public void TryModify(IAtom atom)
    {
        if (enabled && AffectAtom(atom))
            Modify(atom);
    }

    /// <summary>
    /// List of atom types that are not affected by this force.
    /// </summary>
    public List<AtomBehaviour.AtomGroup> ExcludedGroups;

    public virtual void Start() 
    {
        m_groupMask = 0;
        for (int i = 0; i < ExcludedGroups.Count; i++)
        {
            m_groupMask |= (byte)ExcludedGroups[i];
        }
    }

    public virtual bool AffectAtom(IAtom atom)
    {
        return (m_groupMask & (byte)atom.AtomTypeGroup) == 0;
    }

    private byte m_groupMask;
}
