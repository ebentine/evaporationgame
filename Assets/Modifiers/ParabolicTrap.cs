﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Atoms are held by a parabolic surface
/// </summary>
[RequireComponent(typeof(MeshFilter)),RequireComponent(typeof(MeshRenderer))]
public class ParabolicTrap : AtomModifier
{
    /// <summary>
    /// Draw red lines showing the positions of collisions and their normals.
    /// </summary>
    public bool DrawCollisionDebugLines = false;

    /// <summary>
    /// Gravitational acceleration in the trap
    /// </summary>
    public float GravitationalAcceleration = 10f;

    public void Start()
    {
        //CreateMesh();
    }

    public virtual void Update()
    {
        CreateMesh();
    }

    /// <summary>
    /// Radius of the trap edge
    /// </summary>
    public float Radius = 1f;

    public float Steepness = 1f;

    public bool IsInTrapBounds(IAtom atom)
    {
        Vector3 atomPos = atom.Position - transform.position;
        float r2 = Mathf.Pow(atomPos.x, 2) + Mathf.Pow(atomPos.z, 2);
        return (r2 < Radius * Radius);
    }

    public bool IsCollidingWithTrap(IAtom atom)
    {
        Vector3 atomPos = atom.Position - transform.position;
        float r2 = Mathf.Pow(atomPos.x, 2) + Mathf.Pow(atomPos.z, 2);
        return atomPos.y < Steepness * r2;
    }

    public void DoReflection(IAtom atom, float propDistance)
    {
        Vector3 atomPos = atom.Position - transform.position;
        Vector3 velocity = atom.Velocity;
        bool nonspecular = false;

        //Solve equation of parabola and ray intersection to find propagation distance at which we intersected.
        //Only look at negative roots, which are 'behind' the atom's trajectory
        Vector3 impactPoint; float distance;
        bool foundIntercept = GetImpactPoint(atomPos, velocity, out impactPoint, out distance);
        if (Mathf.Abs(distance) < propDistance || !foundIntercept)
        {
            Vector3 normal = GetNormalToParabola(impactPoint);
            //collisions are one-way only (one-way surface, inside parabola)
            float n = Vector3.Dot(normal, velocity.normalized);
            if (n < 0f)
            {
                // perform specular reflection
                Vector3 reflectedVelocity = velocity - 2f * Vector3.Dot(normal, velocity) * normal;

                // some collisions glance with a very low angle. This leads to build up of numerical
                // error and over time causes atoms to become stuck in circular orbits.
                // To mitigate the formation of these, try to push such atoms away from the surface
                // while preserving kinetic energy. Normalisation to preserve ke done elsewhere.
                if (Mathf.Abs(n) < 0.2) // 10 degrees, or there about
                {
                    nonspecular = true;
                    reflectedVelocity += normal * Mathf.Max(3f, velocity.magnitude);
                }

                // propagate to new end position.
                if (float.IsNaN(distance))
                    distance = 0f;
                if (distance < 0f)
                    atom.Position = transform.position + impactPoint + reflectedVelocity.normalized * Mathf.Min(Mathf.Abs(distance), Time.fixedDeltaTime * velocity.magnitude);
                atom.Velocity = reflectedVelocity;

                if (DrawCollisionDebugLines)
                {
                    Color debugColor = foundIntercept ? Color.red : Color.yellow;
                    Debug.DrawLine(atomPos + transform.position - velocity * Time.fixedDeltaTime, atomPos + transform.position, Color.gray, 2f);
                    Debug.DrawLine(atomPos + transform.position, impactPoint + transform.position, Color.black, 2f);
                    Debug.DrawLine(impactPoint + transform.position, impactPoint + transform.position + normal, debugColor, 2f);
                    Debug.DrawLine(impactPoint + transform.position, impactPoint + transform.position + reflectedVelocity * 0.05f, nonspecular ? Color.cyan : Color.green, 2f);
                    Debug.DrawLine(impactPoint + transform.position, impactPoint + transform.position - velocity * 0.05f, Color.blue, 2f);
                }

                //Check for other reflections along the newest propagation length.
                if (distance > 0f)
                    DoReflection(atom, distance);
            }
        }
        else
            Debug.Log("Reflection aborted.");
        
    }

    /// <summary>
    /// Tries to determine the nearest intercept point behind the atom's current position and heading
    /// </summary>
    /// <param name="distance">Distance from atom to intercept point</param>
    /// <param name="intercept">position of intercept</param>
    /// <returns>true if successful</returns>
    public bool GetImpactPoint(Vector3 position, Vector3 velocity, out Vector3 intercept, out float distance)
    {
        Vector3 normVel = velocity.normalized;
        float r2 = Mathf.Pow(position.x, 2) + Mathf.Pow(position.z, 2);
        float r = Mathf.Pow(r2, 0.5f);
        float nvr2 = Mathf.Pow(normVel.x, 2) + Mathf.Pow(normVel.z, 2);
        float nvr = Mathf.Pow(nvr2, 0.5f);
        float nvy = normVel.y;

        float a = nvr2 * Steepness;
        float b = 2 * Steepness * (position.x * normVel.x + position.z * normVel.z) - nvy;
        float c = Steepness * r2 - position.y;

        float beta = Mathf.Pow(b * b - 4 * a * c, 0.5f);
        float alpha1 = (-b + beta) / (2 * a);
        float alpha2 = (-b - beta) / (2 * a);

        if (float.IsNaN(alpha1))
        {
            intercept = GetParabolaSurfacePoint(position);
            distance = float.NaN;
            return false; //can't determine intercept, no solution exists
        }

        // We want the smallest negative alpha.
        distance = (alpha1 < 0 && alpha2 < 0) ? Mathf.Max(alpha1, alpha2) : ((alpha1 < 0) ? alpha1 : alpha2);
        if (distance > 0f)
        {
            intercept = GetParabolaSurfacePoint(position);
            distance = float.NaN;
            return false;
        }
        intercept = position + normVel * distance;
        return true;
    }

    /// <summary>
    /// Gets a point on the parabola surface nearest to point p.
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    public Vector3 GetParabolaSurfacePoint(Vector3 p)
    {
        //With first order correction
        Vector3 normal = GetNormalToParabola(p);

        Vector3 impact;
        float distance;
        GetImpactPoint(p, -normal, out impact, out distance);
        if (float.IsNaN(distance))
            throw new System.Exception("Coul not find surface point.");

        return impact;
    }

    /// <summary>
    /// Gets the normal to the parabola at the specified position.
    /// Uses only radial coordinates, assumes vertical is such that position lies on parabola.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public Vector3 GetNormalToParabola(Vector3 position)
    {
        //Determine the normal at the position of the impact.
        Vector3 normal = new Vector3(-2 * Steepness * position.x, +1, -2 * Steepness * position.z);
        return normal.normalized;
    }

    public override void Modify(IAtom atom)
    {
        if (!IsInTrapBounds(atom))
            atom.IsTrapped = false;

        // Exert spring force
        if (atom.IsTrapped && IsCollidingWithTrap(atom))
        {
            atom.CanCollide = false;

            float v = atom.Velocity.sqrMagnitude;
            DoReflection(atom, float.PositiveInfinity);
            //if (v > 1e-8f)
            atom.Velocity = atom.Velocity.normalized * Mathf.Pow(v, 0.5f);
        }
        else
        {
            atom.CanCollide = true;
        }

        // Gravitational acceleration
        atom.Velocity += GravitationalAcceleration * Vector3.down * Time.fixedDeltaTime;
    }
        

    #region Mesh

    public void CreateMesh()
    {
        // Create vertices

        int thetaN = 30;
        int levels = 20;
        
        List<Vector3> vertices = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        for (int j = 0; j < levels; j++)
        {
            float r = j * Radius / (levels - 1);
            for (int i = 0; i < thetaN; i++)
            {
                float theta = 2f * Mathf.PI * i / thetaN;
                Vector3 vert = new Vector3(Mathf.Cos(theta) * r, Mathf.Pow(r, 2) * Steepness, Mathf.Sin(theta) * r);
                vertices.Add(vert);
                Vector3 normal = new Vector3(2 * Steepness * vert.x, -1, 2 * Steepness * vert.z);
                normals.Add(normal.normalized);
            }
        }

        int startOfOuterLayerVertices = vertices.Count;

        List<int> triangles = new List<int>();
        for (int j = 0; j < levels - 1; j++)
        {
            for (int i = 0; i < thetaN; i++)
            {
                int first = i + j * thetaN;
                int second = (i < thetaN - 1) ? i + j * thetaN + 1 : j * thetaN;
                int third = first + thetaN;
                int fourth = second + thetaN;

                triangles.Add(first); triangles.Add(second); triangles.Add(third);
                triangles.Add(second); triangles.Add(fourth); triangles.Add(third);
            }
        }
        //reverse layer
        for (int j = 0; j < levels - 1; j++)
        {
            for (int i = 0; i < thetaN; i++)
            {
                int first = i + j * thetaN;
                int second = (i < thetaN - 1) ? i + j * thetaN + 1 : j * thetaN;
                int third = first + thetaN;
                int fourth = second + thetaN;

                triangles.Add(third); triangles.Add(second); triangles.Add(first);
                triangles.Add(third); triangles.Add(fourth); triangles.Add(second);
            }
        }

        MeshFilter mf = GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        mf.mesh = mesh;

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.normals = normals.ToArray();
        mesh.RecalculateBounds();

    }

    #endregion

}