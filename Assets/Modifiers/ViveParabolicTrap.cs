﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ViveParabolicTrap : ParabolicTrap
{
    public SteamVR_TrackedObject trackedObj;
    public SteamVR_Controller.Device device;

    public bool IsHoldingTrigger()
    {
        if (device == null)
        {
            device = SteamVR_Controller.Input((int)trackedObj.index);
            return false;
        }
        else
        {
            //Debug.Log(String.Format("Trigger={0}", device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x));
            return (device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x) > 0.8f;
        }
    }

    Vector3 startPos;
    float oldRadius;

    public override void Update()
    {
        //As track object moves, adjust radius if we are holding trigger.
        if (!IsHoldingTrigger())
        {
            startPos = trackedObj.transform.position;
            oldRadius = Radius;
        }
        else
        {
            float distance1 = startPos.magnitude;
            float distance2 = trackedObj.transform.position.magnitude;
            //float sign = (trackedObj.transform.position.sqrMagnitude > startPos.sqrMagnitude) ? 1f : -1f;
            Radius = oldRadius * distance2 / distance1;// *distance * sign;
            Debug.Log(String.Format("KnifeRadius={0}, distance={1}", Radius, distance2));
        }
        base.Update();
    }
}