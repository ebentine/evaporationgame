﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : AtomModifier
{
    public float GravitationalAcceleration = 10f;

    public bool OnlyAffectedUntrappedAtoms = true;

    public override void Modify(IAtom atom)
    {
        // Gravitational acceleration
        atom.Velocity += GravitationalAcceleration * Vector3.down * Time.fixedDeltaTime;
    }

    public override bool AffectAtom(IAtom atom)
    {
        if (atom.IsTrapped && OnlyAffectedUntrappedAtoms)
            return false;
        return base.AffectAtom(atom);
    }
}