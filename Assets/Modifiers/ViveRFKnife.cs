﻿using System.Collections.Generic;
using UnityEngine;

public class ViveRFKnife : RFKnife
{
    public void CheckStartToHoldTrigger()
    {
        foreach (SteamVR_TrackedObject trackedObj in trackedObjs)
        {
            if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                continue;
            var device = SteamVR_Controller.Input((int)trackedObj.index);
            if (device != null)
            {
                if (device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x > 0.8f)
                {
                    controller = trackedObj;
                    controllerDistanceAtStart = (controller.transform.position - transform.position).magnitude;
                    oldRadius = KnifeRadius;
                    _holdingTrigger = true;
                    return;
                }
            }
        }
    }

    public bool IsHoldingTrigger()
    {
        var device = SteamVR_Controller.Input((int)controller.index);
        if (device == null)
            return false;
        return device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x > 0.8f;
    }

    public override void UpdateRadius()
    {
        if (_holdingTrigger)
        {
            float currentControllerDistance = (controller.transform.position - transform.position).magnitude;
            KnifeRadius = oldRadius * currentControllerDistance / controllerDistanceAtStart;
            var device = SteamVR_Controller.Input((int)controller.index);
            device.TriggerHapticPulse(100);
            _holdingTrigger = IsHoldingTrigger();
        }
        else
            CheckStartToHoldTrigger();
    }

    #region Adding/Removing VR controllers

    public override void Start()
    {
        base.Start();
        trackedObjs = new List<SteamVR_TrackedObject>();
    }

    void OnDeviceConnected(int i, bool connected) {
        Debug.Log("Detected new device connected.");
        trackedObjs.Clear();
        trackedObjs.AddRange(FindObjectsOfType<SteamVR_TrackedObject>());
    }

    public void OnEnable()
    {
        SteamVR_Events.DeviceConnected.Listen(OnDeviceConnected);
    }

    public void OnDisable()
    {
        SteamVR_Events.DeviceConnected.Remove(OnDeviceConnected);
    }

    #endregion

    Vector3 controllerPositionAtStartHoldTrigger;
    float controllerDistanceAtStart;
    float oldRadius;
    private List<SteamVR_TrackedObject> trackedObjs;
    private SteamVR_TrackedObject controller = null;
    private bool _holdingTrigger = false;
}