﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualParabolicTrap : ParabolicTrap
{
    public override void Update()
    {
        float rate = Input.GetAxis("Vertical");
        Radius = Mathf.Max(Radius - Time.fixedDeltaTime * rate, 0.1f);
        base.Update();
    }
}