﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Exerts a harmonic confinement on atoms that are trapped
/// </summary>
public class HarmonicTrap : AtomModifier, IPotential
{
    public float SpringConstant = 3.0f;

    public override void Modify(IAtom atom)
    {
        // Exert spring force
        atom.Velocity -= SpringConstant * (atom.Position - transform.position) * Time.fixedDeltaTime;
    }

    public override bool AffectAtom(IAtom atom)
    {
        if (!atom.IsTrapped)
            return false;
        return base.AffectAtom(atom);
    }

    /// <summary>
    /// Calculate the potential energy of an atom in the harmonic trap.
    /// </summary>
    public float CalculatePotentialEnergy(IAtom atom)
    {
        return atom.Position.sqrMagnitude * SpringConstant / 2f;
    }

    /// <summary>
    /// Gets number of degrees of freedom (3 as potential is 3D)
    /// </summary>
    public int DegreesOfFreedom { get { return 3; } }
}