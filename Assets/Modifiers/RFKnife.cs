﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RFKnife : AtomModifier
{
    /// <summary>
    /// Initial cut radius of the rf knife.
    /// </summary>
    public float InitialKnifeRadius = 3.0f;

    /// <summary>
    /// Radius of the rf knife.
    /// </summary>
    public float KnifeRadius = 0.0f;

    /// <summary>
    /// Final cut the rf will go to.
    /// </summary>
    public float FinalCut = 1.0f;

    /// <summary>
    /// Initial hold time before rf knife starts cutting.
    /// </summary>
    public float InitialPause = 2.0f;

    /// <summary>
    /// Rate of decrease of radius of rf knife.
    /// </summary>
    public float CutRate = 0.1f;

    void Awake()
    {
    }

    public void FixedUpdate()
    {
        UpdateRadius();
        transform.localScale = Vector3.one * KnifeRadius * 2f;
    }

    /// <summary>
    /// Resets the rf knife.
    /// </summary>
    public void Reset()
    {
        KnifeRadius = InitialKnifeRadius;
        transform.localScale = Vector3.one * KnifeRadius * 2f;
    }

    public override void Start()
    {
        base.Start();
        Reset();
    }

    public virtual void UpdateRadius()
    {
        KnifeRadius = Mathf.Max(KnifeRadius - Time.fixedDeltaTime * CutRate, FinalCut);
    }

    public override void Modify(IAtom atom)
    {
        if (atom.Position.magnitude > KnifeRadius)
            atom.IsTrapped = false;
    }
}