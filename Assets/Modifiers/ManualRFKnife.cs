﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualRFKnife : RFKnife
{

    /// <summary>
    /// Manual knife updates radius based on mouse input.
    /// </summary>
    /// <param name="dt"></param>
    public override void UpdateRadius()
    {
        if (PlayerControlled)
        {
            float rate = Input.GetAxis("Vertical");
            KnifeRadius = Mathf.Max(KnifeRadius - Time.fixedDeltaTime * rate, 0.1f);
        }
        else
            base.UpdateRadius();
    }

    /// <summary>
    /// Does player currently control the knife?
    /// </summary>
    public bool PlayerControlled = true;
}