﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a natural 1/e decay of atoms in the trap
/// </summary>
public class Decay : AtomModifier
{
    public float Lifetime = 100f;

    public override void Modify(IAtom atom)
    {
        //Random chance to no longer be trapped.
        if (Random.value > Mathf.Exp(-Time.fixedDeltaTime / Lifetime))
            atom.IsTrapped = false;
    }

    public override bool AffectAtom(IAtom atom)
    {
        if (!atom.IsTrapped)
            return false;
        return base.AffectAtom(atom);
    }
}