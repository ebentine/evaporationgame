﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Exerts a harmonic confinement on atoms that are trapped
/// </summary>
public class LinearTrap : AtomModifier
{
    public float TrapStrength = 3.0f;

    public override void Modify(IAtom atom)
    {
        // Exert trap force
        atom.Velocity -= TrapStrength * (atom.Position - transform.position).normalized * Time.fixedDeltaTime;
    }

    public override bool AffectAtom(IAtom atom)
    {
        if (!atom.IsTrapped)
            return false;
        return base.AffectAtom(atom);
    }
}