﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BECTransition : MonoBehaviour {

    private PSDMeasurable _PSD;
    private MeshRenderer _MeshRenderer;
    private float _becSize = 0f;

	// Use this for initialization
	void Start () {
        _PSD = FindObjectOfType<PSDMeasurable>();
        _MeshRenderer = GetComponent<MeshRenderer>();
        Reset();
        _PSD.TransitionOccurs += _PSD_TransitionOccurs;
	}

    void _PSD_TransitionOccurs(PSDMeasurable obj)
    {
        ShowBEC();
    }

    public void Reset()
    {
        _MeshRenderer.enabled = false;
        _showing = false;
        _scalecounter = 0f;
        transform.localScale = Vector3.one * _scalecounter;
    }

    bool _showing = false;
    float _scalecounter;

    void ShowBEC()
    {
        if (_showing)
            return;
        _MeshRenderer.enabled = true;
        _showing = true;
        _scalecounter = 0f;
        transform.localScale = Vector3.zero;
        _becSize = 0.2f * Mathf.Pow(FindObjectOfType<AtomCloudTimeIntegrator>().Atoms.Count(atom => atom.IsTrapped), 1f / 2f) / 2f;
    }
	
	// Update is called once per frame
	void Update () {
		
        if (_showing)
        {
            _scalecounter = Mathf.Min(_scalecounter + 2f * Time.deltaTime, 1f);
            transform.localScale = Vector3.one * _scalecounter * _becSize;
        }
	}
}
