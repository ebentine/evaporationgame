using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DensityMeasurement : MonoBehaviour {

  // Use this for initialization
  void Start () {
		
  }

  public void Reset()
  {
    Renderer render = GetComponent<Renderer>();
    if (render != null)
      render.material.color = Color.white;
  }

  public int MinCount = 10;
  public float Density;
  float lastUpdate = -100f;

	
  // Update is called once per frame

  void Update () {
    {
      if (Time.time < lastUpdate + 0.5f)
	return;

      lastUpdate = Time.time;

      //Get all atoms
      int numberAtoms = 0;
      foreach (AtomBehaviour a in FindObjectsOfType<AtomBehaviour>())
        {
	  if (!a.IsTrapped)
	    continue;
	  numberAtoms++;
        }

      //Get trap radius
      float trapRadius = 0.0f;
      foreach (RFKnife trap in FindObjectsOfType<RFKnife>())
        {
	  trapRadius = trap.KnifeRadius;
	}

      //Calculate density
      float Density = numberAtoms / Mathf.Pow(trapRadius, 2);

      TextMesh tm = GetComponent<TextMesh>();
      if (tm != null)
        {

	  tm.text = "Density: " + Density.ToString("000.0");
	  if (numberAtoms < MinCount)
            {
	      Renderer render = GetComponent<Renderer>();
	      if (render != null)
		render.material.color = Color.gray;
            }
        }
    }
  }

		
}
