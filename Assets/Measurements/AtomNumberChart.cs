﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomNumberChart : TimeVaryingEnsembleProperty {

    public override float Calculate(IAtom atom)
    {
        return 1f;
    }

}
