﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// A chart showing the temperature of atoms
/// </summary>
public class TemperatureChart : TimeVaryingEnsembleProperty
{
    IEnumerable<IPotential> _potentials;

    public override void Start()
    {
        base.Start();
        _potentials = GameObject.FindObjectsOfType<AtomModifier>()
            .Where(a => a is IPotential).Cast<IPotential>();
    }

    public override float Calculate(IAtom atom)
    {
        return atom.Mass * atom.Velocity.sqrMagnitude / 2 / 3 + _potentials.Sum(pot => pot.CalculatePotentialEnergy(atom) / pot.DegreesOfFreedom);
    }
}
