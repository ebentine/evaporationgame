﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// A graph comprising various measurable properties.
/// </summary>
[RequireComponent(typeof(MeshFilter)), RequireComponent(typeof(MeshRenderer))]
public class MeasurableGraph : MonoBehaviour
{
    [Tooltip("Quantity to use as X axis")]
    public MeasurableQuantity XMeasurable;

    [Tooltip("Quantity to use as Y axis")]
    public MeasurableQuantity YMeasurable;

    /// <summary>
    /// Real-space width of lines
    /// </summary>
    [Tooltip("Real-space width of lines.")]
    public float LineWidth = 0.1f;

    [Tooltip("Real-space width of axis lines.")]
    public float AxesLineWidth = 0.1f;

    /// <summary>
    /// Real-space width of the axes object
    /// </summary>
    [Tooltip("Real-space width of the axes object.")]
    public float AxisWidth = 5f;

    /// <summary>
    /// Real-space height of the axes object
    /// </summary>
    [Tooltip("Real-space height of the axes object.")]
    public float AxisHeight = 2f;

    [Tooltip("Don't add new vertices unless they are greater than this distance from previous.")]
    public float VertexDistanceThreshold = 0.01f;

    /// <summary>
    /// Update rate /s
    /// </summary>
    public float UpdateRate = 5f;

    public Color PlotColor = Color.red;

    /// <summary>
    /// Represents an individual series on the line graph.
    /// </summary>
    public class LineGraphSeries
    {
        public LineGraphSeries()
        {
            Data = new List<Vector2>();
        }

        /// <summary>
        /// x,y coordinates for data in the graph, assumed sorted along x.
        /// </summary>
        public List<Vector2> Data { get; set; }

        /// <summary>
        /// Color to plot the series in.
        /// </summary>
        public Color Color { get; set; }
    }

    /// <summary>
    /// Series to be plotted in this linegraph object.
    /// </summary>
    public List<LineGraphSeries> Series { get; protected set; }

    public virtual void Start()
    {
        Series = new List<LineGraphSeries>();
        if (XMeasurable == null)
        {
            enabled = false;
            throw new Exception("A Measurable is null");
        }
        Series.Add(new LineGraphSeries() { Color = PlotColor });
        _mesh = new Mesh();
        BuildInitialMesh();
    }

    public void Reset()
    {
        foreach (var s in Series)
            s.Data.Clear();

        BuildInitialMesh();
    }

    private float _updateTimer;

    public virtual void Update()
    {
        _updateTimer += Time.deltaTime;
        if (_updateTimer < 1 / UpdateRate)
            return;
        _updateTimer = 0f;

        //Calculate values for both x and y.
        float x = XMeasurable.Measure();
        float y = YMeasurable.Measure();
        if (!float.IsNaN(x) && !float.IsNaN(y))
            Series[0].Data.Add(new Vector2(x, y));
        //BuildInitialMesh();
        AppendSeriesMesh(Series[0]);
    }

    #region Mesh

    private List<Vector3> _vertices = new List<Vector3>(65535);
    private List<Vector3> _normals = new List<Vector3>(65535);
    private List<Color> _colors = new List<Color>(65535);
    private List<int> _triangles = new List<int>(65535);


    /// <summary>
    /// Creates mesh for a given series of the line graph.
    /// </summary>
    /// <param name="series">Series to create mesh for</param>
    public void AppendSeriesMesh(LineGraphSeries series)
    {
        int dCount = series.Data.Count;

        Vector2 next = series.Data[dCount - 1];

        Vector2 xlim = XMeasurable.GetPlotRange(series.Data.Select(v => v.x).ToArray());
        Vector2 ylim = YMeasurable.GetPlotRange(series.Data.Select(v => v.y).ToArray());

        float nyPos, nxPos;

        // n_Pos are normalised x/y coordinate [0,1] which maps values to min/max range of graph.
        nyPos = (next.y - ylim.x) / (ylim.y - ylim.x);
        nxPos = (next.x - xlim.x) / (xlim.y - xlim.x);

        Vector3 vertex1 = new Vector3(nxPos * AxisWidth, nyPos * AxisHeight - LineWidth / 2, 0);
        Vector3 vertex2 = new Vector3(nxPos * AxisWidth, nyPos * AxisHeight + LineWidth / 2, 0);

        //Only add vertices if sufficiently different to previous
        if ((vertex2 - _vertices[_vertices.Count - 1]).sqrMagnitude < Mathf.Pow(VertexDistanceThreshold, 2))
            return;

        _vertices.Add(vertex1);
        _colors.Add(series.Color);
        _vertices.Add(vertex2);
        _colors.Add(series.Color);

        if (dCount < 2) // can't draw triangles until at least two data points are recorded
            return;

        int v0 = _vertices.Count - 4;
        // Only plot triangles if within axes bounds
        if (!(nyPos >= -0.1f && nyPos <= 1.1f && nxPos >= -0.1f && nxPos <= 1.1f))
            return;

        _triangles.Add(v0); _triangles.Add(v0 + 2); _triangles.Add(v0 + 1);
        _triangles.Add(v0 + 1); _triangles.Add(v0 + 2); _triangles.Add(v0 + 3);

        _mesh.SetVertices(_vertices);
        _mesh.SetTriangles(_triangles, 0);
        _mesh.SetNormals(_normals);
        _mesh.SetColors(_colors);
        Debug.Log("_vertices[0]=" + _vertices[v0]);
    }

    public void BuildInitialMesh()
    {
        _vertices.Clear();
        _colors.Clear();
        _triangles.Clear();
        _normals.Clear();

        //add the axes
        _vertices.Add(Vector3.zero + Vector3.up * AxesLineWidth / 2f);
        _vertices.Add(Vector3.zero - Vector3.up * AxesLineWidth / 2f);
        _vertices.Add(Vector3.right * AxisWidth + Vector3.up * AxesLineWidth / 2f);
        _vertices.Add(Vector3.right * AxisWidth - Vector3.up * AxesLineWidth / 2f);
        _colors.Add(Color.white); _colors.Add(Color.white); _colors.Add(Color.white); _colors.Add(Color.white);
        _triangles.AddRange(new int[] { 0, 2, 3, 3, 1, 0 });

        _vertices.Add(Vector3.zero + Vector3.left * AxesLineWidth / 2f);
        _vertices.Add(Vector3.zero - Vector3.left * AxesLineWidth / 2f);
        _vertices.Add(Vector3.up * AxisHeight + Vector3.left * AxesLineWidth / 2f);
        _vertices.Add(Vector3.up * AxisHeight - Vector3.left * AxesLineWidth / 2f);
        _colors.Add(Color.white); _colors.Add(Color.white); _colors.Add(Color.white); _colors.Add(Color.white);
        _triangles.AddRange(new int[] { 4, 6, 7, 7, 5, 4 });

        // Create mesh for each series.
        //foreach (LineGraphSeries series in Series)
        //    CreateSeriesMesh(series, ref vertices, ref normals, ref colors, ref triangles);

        MeshFilter mf = GetComponent<MeshFilter>();
        mf.mesh = _mesh;
        _mesh.SetVertices(_vertices);
        _mesh.SetTriangles(_triangles, 0);
        _mesh.SetNormals(_normals);
        _mesh.SetColors(_colors);
        _mesh.RecalculateBounds();
    }

    private Mesh _mesh;

    #endregion

}
