﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TimeVaryingEnsembleProperty : LineChart {

    [Tooltip("Only include trapped atoms in the calculation.")]
    public bool OnlyAverageTrapped = true;

    [Tooltip("Don't calculate quantities when less than this many atoms.")]
    public int RequiredNumber = 10;

    [Tooltip("Should the quantity be averaged?")]
    public bool Average = true;

    /// <summary>
    /// Calculate the ensemble property for a single atom
    /// </summary>
    public virtual float Calculate(IAtom atom)
    {
        return atom.Position.x;
    }

    public override void Start()
    {
        Series = new List<LineGraphSeries>();
        m_seriesLookup = new Dictionary<AtomBehaviour.AtomGroup, LineGraphSeries>();
        Integrator = FindObjectOfType<AtomCloudTimeIntegrator>();
    }

    public AtomCloudTimeIntegrator Integrator { get; private set; }
    private Dictionary<AtomBehaviour.AtomGroup, LineGraphSeries> m_seriesLookup;

    public override void CalculateQuantities()
    {
        if (Integrator == null)
        {
            Debug.LogError("Could not find integrator.");
            enabled = false;
            return;
        }

        foreach (AtomBehaviour.AtomGroup group in Integrator.Groups())
        {
            List<IAtom> atoms = Integrator.GetAtomsByGroup(group);

            if (atoms.Count == 0)
                continue;

            List<IAtom> test = new List<IAtom>(atoms.Where(a => OnlyAverageTrapped ? a.IsTrapped : true));
            if (test.Count < RequiredNumber)
                continue;
            float s = test.Sum(b => Calculate(b));

            if (Average)
                s = s / test.Count;

            // Create series if it does not exist
            if (!m_seriesLookup.ContainsKey(group))
            {
                LineGraphSeries series = new LineGraphSeries();
                series.Color = (atoms[0] as AtomBehaviour).TrappedColor;
                m_seriesLookup.Add(group, series);
                Series.Add(series);
            }

            m_seriesLookup[group].Data.Add(new Vector2(Time.fixedTime, s));
        }
    }
}
