﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A LineChart plots quantities over time.
/// </summary>
public class LineChart : LineGraph {

    /// <summary>
    /// Time to plot, in seconds
    /// </summary>
    [Tooltip("Time to plot, in seconds")]
    public float ChartTime = 10f;

	public override void Update () {
		float currentTime = Time.fixedTime;
        float cutoff = currentTime - ChartTime;
        XAxisLimits = new Vector2(cutoff, currentTime);
        
        //remove previous data points
        foreach (LineGraphSeries series in Series)
        {
           series.Data.RemoveAll(p => p.x < cutoff);
        }

        //Add new quantities
        CalculateQuantities();
        CreateMesh();
	}

    public virtual void CalculateQuantities()
    {

    }

    public virtual void Clear()
    {
        Series.ForEach(s => s.Data.Clear());
    }
}
