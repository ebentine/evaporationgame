﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// make 1D graph.
/// </summary>
[RequireComponent(typeof(MeshFilter)),RequireComponent(typeof(MeshRenderer))]
public class LineGraph : MonoBehaviour
{
    /// <summary>
    /// Real-space width of lines
    /// </summary>
    [Tooltip("Real-space width of lines.")]
    public float LineWidth = 0.3f;

    [Tooltip("Real-space width of axis lines.")]
    public float AxesLineWidth = 0.1f;

    /// <summary>
    /// Real-space width of the axes object
    /// </summary>
    [Tooltip("Real-space width of the axes object.")]
    public float AxisWidth = 5f;

    /// <summary>
    /// Real-space height of the axes object
    /// </summary>
    [Tooltip("Real-space height of the axes object.")]
    public float AxisHeight = 2f;

    /// <summary>
    /// X-axis limits of the plot range
    /// </summary>
    [Tooltip("X-axis limits of the plot range. Use Nan for auto scale.")]
    public Vector2 XAxisLimits = new Vector2(float.NaN, float.NaN);

    /// <summary>
    /// Y-axis limits of the plot range
    /// </summary>
    [Tooltip("Y-axis limits of the plot range. Use Nan for auto scale.")]
    public Vector2 YAxisLimits = new Vector2(float.NaN, float.NaN);

    /// <summary>
    /// Represents an individual series on the line graph.
    /// </summary>
    public class LineGraphSeries
    {
        public LineGraphSeries()
        {
            Data = new List<Vector2>();
        }

        /// <summary>
        /// x,y coordinates for data in the graph, assumed sorted along x.
        /// </summary>
        public List<Vector2> Data { get; set; }

        /// <summary>
        /// Color to plot the series in.
        /// </summary>
        public Color Color { get; set; }
    }

    /// <summary>
    /// Series to be plotted in this linegraph object.
    /// </summary>
    public List<LineGraphSeries> Series { get; protected set; }

    public virtual void Start()
    {
        Series = new List<LineGraphSeries>();
    }

    public virtual void Update()
    {
        //Series.Clear();
        //LineGraphSeries s = new LineGraphSeries();
        //s.Color = Color.red;
        //s.Data = new List<Vector2>();
        //for (int i = 0; i < 40; i++)
        //    s.Data.Add(new Vector2(i, Mathf.Sin(i - Time.fixedTime)));
        //Series.Add(s);
        //CreateMesh();
    }

    #region Mesh

    /// <summary>
    /// Returns the axis limits. 
    /// </summary>
    /// <param name="x">Calculate limit for x axis?</param>
    public Vector2 GetAxisLimits(bool x)
    {
        //if either limit is nan, enumerate to find min or max.
        float max = x ? XAxisLimits.y : YAxisLimits.y;
        if (float.IsNaN(max))
        {
            max = float.NegativeInfinity;
            foreach (LineGraphSeries s in Series)
                max = Mathf.Max(s.Data.Max((Vector2 v) => x ? v.x : v.y), max);
        }

        float min = x ? XAxisLimits.x : YAxisLimits.x;
        if (float.IsNaN(min))
        {
            min = float.PositiveInfinity;
            foreach (LineGraphSeries s in Series)
                min = Mathf.Min(s.Data.Min((Vector2 v) => x ? v.x : v.y), min);
        }

        return new Vector2(min, max);
    }

    /// <summary>
    /// Creates mesh for a given series of the line graph.
    /// </summary>
    /// <param name="series">Series to create mesh for</param>
    public void CreateSeriesMesh(LineGraphSeries series, out List<Vector3> vertices, out List<Vector3> normals, out List<Color> colors, out List<int> triangles)
    {
        Vector2[] data = series.Data.OrderBy(p=>p.x).ToArray();
        vertices = new List<Vector3>();
        normals = new List<Vector3>();
        colors = new List<Color>();
        triangles = new List<int>();

        Vector2 xlim = GetAxisLimits(true);
        Vector2 ylim = GetAxisLimits(false);

        float nyPos, nxPos;
        for (int j = 0; j < data.Length; j++)
        {
            // n_Pos are normalised x/y coordinate [0,1] which maps values to min/max range of graph.
           nyPos = (data[j].y - ylim.x) / (ylim.y - ylim.x);
           nxPos = (data[j].x - xlim.x) / (xlim.y - xlim.x);

           vertices.Add(new Vector3(nxPos * AxisWidth, nyPos * AxisHeight - LineWidth / 2, 0));
           colors.Add(series.Color);
           vertices.Add(new Vector3(nxPos * AxisWidth, nyPos * AxisHeight + LineWidth / 2, 0));
           colors.Add(series.Color);
        }

        for (int j = 0; j < vertices.Count - 3; j++)
        {
            triangles.Add(j); triangles.Add(j + 1); triangles.Add(j + 2);
            triangles.Add(j + 1); triangles.Add(j + 3); triangles.Add(j + 2);
        }
    }

    public void CreateMesh()
    {
        List<Vector3> vertices = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        List<Color> colors = new List<Color>();
        List<int> triangles = new List<int>();;
        List<Vector3> v, n;
        List<Color> c;
        List<int> t;

        //add the axes
        vertices.Add(Vector3.zero + Vector3.up * AxesLineWidth / 2f);
        vertices.Add(Vector3.zero - Vector3.up * AxesLineWidth / 2f);
        vertices.Add(Vector3.right * AxisWidth + Vector3.up * AxesLineWidth / 2f);
        vertices.Add(Vector3.right * AxisWidth - Vector3.up * AxesLineWidth / 2f);
        colors.Add(Color.white); colors.Add(Color.white); colors.Add(Color.white); colors.Add(Color.white);
        triangles.AddRange(new int[] { 0, 2, 3, 3, 1, 0 });

        vertices.Add(Vector3.zero + Vector3.left * AxesLineWidth / 2f);
        vertices.Add(Vector3.zero - Vector3.left * AxesLineWidth / 2f);
        vertices.Add(Vector3.up * AxisHeight + Vector3.left * AxesLineWidth / 2f);
        vertices.Add(Vector3.up * AxisHeight - Vector3.left * AxesLineWidth / 2f);
        colors.Add(Color.white); colors.Add(Color.white); colors.Add(Color.white); colors.Add(Color.white);
        triangles.AddRange(new int[] { 4, 6, 7, 7, 5, 4 });

        // Create mesh for each series.
        int offset = vertices.Count;
        int stagger = 0;
        foreach (LineGraphSeries series in Series)
        {
            CreateSeriesMesh(series, out v, out n, out c, out t);
            for (int i = 0; i < v.Count; i++)
                v[i] = v[i] + 0.1f * stagger * Vector3.back;
            stagger++;
            vertices.AddRange(v);
            normals.AddRange(n);
            colors.AddRange(c);
            for (int i = 0; i < t.Count; i++)
                t[i] = t[i] + offset;
            triangles.AddRange(t);
            offset += v.Count;
        }

        MeshFilter mf = GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        mf.mesh = mesh;
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.normals = normals.ToArray();
        mesh.colors = colors.ToArray();
        mesh.RecalculateBounds();
        //mesh.bounds = new Bounds(transform.position, new Vector3(AxisWidth, AxisHeight, 0f));
    }

    #endregion
}