﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

public class TimeMeasurable : MeasurableQuantity, IMeasurable
{
    [Tooltip("If true, time axis will scroll. Min bound becomes length of time to retain.")]
    public bool UseChart = false;

    public override Vector2 GetPlotRange(float[] data)
    {
        if (UseChart)
            return new Vector2(Time.time - AxisLimits.x, Time.time);
        return base.GetPlotRange(data);
    }

    public override float Measure()
    {
        return Time.time;
    }
}

