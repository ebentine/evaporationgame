﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Represents a quantity that can be measured.
/// </summary>
public interface IMeasurable
{
    float Measure();
}
