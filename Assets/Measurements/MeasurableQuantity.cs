﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Base class for a measurable quantity that can be measured from an atom cloud.
/// </summary>
public abstract class MeasurableQuantity : MonoBehaviour, IMeasurable
{
    public AtomCloudTimeIntegrator Integrator { get; private set; }

    public virtual void Start()
    {
        Integrator = FindObjectOfType<AtomCloudTimeIntegrator>();
        if (Integrator == null)
        {
            Debug.LogError("Could not find atom cloud integrator.");
            enabled = false;
        }
    }

    [Tooltip("Plot range (X=min, Y=max). Use NaN for automatic.")]
    public Vector2 AxisLimits = new Vector2(float.NaN, float.NaN);

    public abstract float Measure();

    /// <summary>
    /// Get the plot range for this quantity, given data
    /// </summary>
    /// <param name="data">1D array of this quantity's values</param>
    /// <returns>Vector with X=min, Y=max</returns>
    public virtual Vector2 GetPlotRange(float[] data)
    {
        float max = AxisLimits.y;
        if (float.IsNaN(max))
            max = data.Max();
        float min = AxisLimits.x;
        if (float.IsNaN(min))
            min = data.Min();
        return new Vector2(min, max);
    }
}
