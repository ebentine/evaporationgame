﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

public class AtomNumberMeasurable : MeasurableQuantity, IMeasurable
{
    [Tooltip("Only count trapped atoms?")]
    public bool CountTrappedOnly = true;

    public override float Measure()
    {
        if (Integrator == null)
        {
            Debug.LogError("Could not find integrator.");
            enabled = false;
            return float.NaN;
        }

        //determine density of the cloud.
        if (CountTrappedOnly)
            return Integrator.Atoms.Count(atom => atom.IsTrapped);
        return Integrator.Atoms.Count;
    }
}