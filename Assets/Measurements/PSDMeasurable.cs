﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

public class PSDMeasurable : MeasurableQuantity, IMeasurable
{
    [Tooltip("Prefactor for PSD.")]
    public float Prefactor = 1f;

    [Tooltip("Should we take log of quantity?")]
    public bool TakeLog = true;

    public float Transition = 5;

    /// <summary>
    /// Don't calculate if we have less than this atom number
    /// </summary>
    [Tooltip("Don't calculate if we have less than this atom number trapped")]
    public int TrappedAtomNumberThreshold = 20;

    IEnumerable<IPotential> _potentials;

    public override void Start()
    {
        base.Start();
        _potentials = GameObject.FindObjectsOfType<AtomModifier>()
            .Where(a => a is IPotential).Cast<IPotential>();
    }

    public override float Measure()
    {
        if (Integrator == null)
        {
            Debug.LogError("Could not find integrator.");
            enabled = false;
            return float.NaN;
        }

        IEnumerable<IAtom> trapped = Integrator.Atoms.Where(atom => atom.IsTrapped);

        if (Integrator.Atoms.Count(a => a.IsTrapped) < TrappedAtomNumberThreshold)
            return float.NaN;

        //determine density of the cloud.
        int atomNum = trapped.Count();
        float vol = Mathf.Pow(trapped.Sum(a => a.Position.sqrMagnitude) / atomNum, 3f / 2f);
        float density = atomNum / vol;

        //calculate temperature via equipartition.
        float dof = 3 + _potentials.Sum(pot => pot.DegreesOfFreedom);
        float T = trapped.Sum(
            atom => atom.Mass * atom.Velocity.sqrMagnitude / 2f + _potentials.Sum(pot => pot.CalculatePotentialEnergy(atom))
                ) / (atomNum * dof);

        // Note: Not properly treating mass!
        float psd = Prefactor * density * Mathf.Pow(T, -3f / 2f);
        
        if (TakeLog)
            psd = Mathf.Log10(psd);

        if (psd > Transition)
            TransitionOccurs(this);

        return psd;
    }

    public event Action<PSDMeasurable> TransitionOccurs;
}

