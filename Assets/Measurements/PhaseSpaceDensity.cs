﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

public class PhaseSpaceDensity : LineChart
{
    [Tooltip("Don't calculate quantities when less than this many atoms.")]
    public int RequiredNumber = 10;

    [Tooltip("Prefactor for PSD.")]
    public float Prefactor = 1f;
    
    public AtomCloudTimeIntegrator Integrator { get; private set; }

    IEnumerable<IPotential> _potentials;

    public override void Start()
    {
        Series = new List<LineGraphSeries>();
        Series.Add(new LineGraphSeries() { Color = Color.red });
        Integrator = FindObjectOfType<AtomCloudTimeIntegrator>();

        _potentials = GameObject.FindObjectsOfType<AtomModifier>()
            .Where(a => a is IPotential).Cast<IPotential>();
    }

    public override void CalculateQuantities()
    {
        if (Integrator == null)
        {
            Debug.LogError("Could not find integrator.");
            enabled = false;
            return;
        }

        //determine density of the cloud.
        int atomNum = Integrator.Atoms.Count;
        float vol = Mathf.Pow(Integrator.Atoms.Sum(a => a.Position.sqrMagnitude) / atomNum, 3f / 2f);
        float density = atomNum / vol;

        //calculate temperature via equipartition.
        //  Note: This expression wont work for anything other than harmonic trap.
        float T = Integrator.Atoms.Sum(
            atom => atom.Mass * atom.Velocity.sqrMagnitude / 2f / 3f + _potentials.Sum(pot => pot.CalculatePotentialEnergy(atom) / pot.DegreesOfFreedom)
                ) / atomNum;

        // Note: Not properly treating mass!
        float psd = density * Mathf.Pow(T, -3f / 2f);

        Series[0].Data.Add(new Vector2(Time.fixedTime, psd)); 
    }
}