﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Manages a series of presentation items.
/// </summary>
public class PresentationMgr : MonoBehaviour
{
    public string StartCue = "Start";

    private Dictionary<string, List<PresentationItem>> _Triggers = null;

    public void Start()
    {
        StartCoroutine(StartPresentation());
    }

    public void Register(PresentationItem item)
    {
        if (_Triggers == null)
            _Triggers = new Dictionary<string, List<PresentationItem>>();

        if (!_Triggers.ContainsKey(item.TriggerCue))
            _Triggers.Add(item.TriggerCue, new List<PresentationItem>());

        _Triggers[item.TriggerCue].Add(item);
        Debug.Log("Added cue=" + item.TriggerCue);
    }

    public IEnumerator StartPresentation()
    {
        yield return new WaitForSeconds(0.1f);
        Trigger(StartCue);
    }

    public void Trigger(string cue)
    {
        if (_Triggers == null)
            return;

        if (!_Triggers.ContainsKey(cue))
        {
            Debug.LogWarning("Could not find cue=" + cue);
            return;
        }

        if (cue == "")
        {
            Debug.LogWarning("Empty cue in Trigger.");
            return;
        }

        foreach (PresentationItem item in _Triggers[cue])
        {
            item.Trigger();
        }
        Debug.Log("Triggered cue=" + cue);
    }
}
