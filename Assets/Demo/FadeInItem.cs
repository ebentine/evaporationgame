﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An item of text that can fade in or out.
/// </summary>
[RequireComponent(typeof(MeshRenderer))]
public class FadeInItem : PresentationItem {

    public float FadeInDuration = 1f;
    public float FadeOutDuration = 0.5f;
    public float ShowDuration = 1f;
    private MeshRenderer _meshRenderer;
    private Color _initialColor;
    public bool DontFadeOut = false;

    public override void Trigger()
    {
        base.Trigger();
        _fadeTimer = 0f;
        _running = true;
        _initialColor = _meshRenderer.material.color;
    }

    public override void Start()
    {
        base.Start();
        _meshRenderer = transform.GetComponent<MeshRenderer>();
        if (_meshRenderer == null)
            throw new Exception("Cannot find MeshRenderer.");
        _meshRenderer.enabled = false;
    }

    public override void Update()
    {
        base.Update();

        if (!_running)
            return;

        _fadeTimer += Time.deltaTime;
        _meshRenderer.material.color = _initialColor * GetFadeColor();
        _meshRenderer.enabled = true;
        if (_fadeTimer > FadeInDuration + FadeOutDuration + ShowDuration && !DontFadeOut)
        {
            _fadeTimer = 0f;
            _running = false;
            _meshRenderer.enabled = false;
        }
    }

    public Color GetFadeColor()
    {
        if (_fadeTimer < FadeInDuration)
            return new Color(1f, 1f, 1f, _fadeTimer / FadeInDuration);
        float nt = (_fadeTimer - FadeInDuration - ShowDuration) / FadeOutDuration;
        if (nt > 0f && !DontFadeOut)
            return new Color(1f, 1f, 1f, Mathf.Max(0f, 1f-nt));
        return Color.white;
    }

    private float _fadeTimer;
    private bool _running;
}