﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawSpringsPresentationItem : PresentationItem {

    public override void Start()
    {
        base.Start();
        _drawSpringsTimer = 0f;

        _integrator = FindObjectOfType<AtomCloudTimeIntegrator>();
        if (_integrator == null)
            throw new Exception("Could not find integrator.");
    }

    private AtomCloudTimeIntegrator _integrator;

    public float DrawSpringsFor = 4f;

    public override void Trigger()
    {
        base.Trigger();
        _drawSpringsTimer = DrawSpringsFor;
    }

            /// <summary>
    /// Should we draw spring forces on objects?
    /// </summary>
    private float _drawSpringsTimer = 0f;

    public override void Update()
    {
        base.Update();

        if (_drawSpringsTimer < 0f)
            return;

        _drawSpringsTimer -= Time.deltaTime;

        _integrator.Atoms.ForEach(a => Debug.DrawLine(a.Position, Vector3.zero, Color.red, 0));
    }
}
