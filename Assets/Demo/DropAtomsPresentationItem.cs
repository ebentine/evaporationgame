﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Drops atoms when triggered.
/// </summary>
public class DropAtomsPresentationItem : PresentationItem
{
    private AtomCloudTimeIntegrator _integrator;

    public override void Start()
    {
        base.Start();
        _integrator = FindObjectOfType<AtomCloudTimeIntegrator>();
        if (_integrator == null)
            throw new Exception("Could not find integrator.");
    }

    public override void Trigger()
    {
        base.Trigger();
        _integrator.Atoms.ForEach(a => a.IsTrapped = false);
    }
}