﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Drops atoms when triggered.
/// </summary>
public class EnablePresentationItem : PresentationItem
{
    public MonoBehaviour component;

    public override void Start()
    {
        base.Start();
    }

    public override void Trigger()
    {
        base.Trigger();
        component.enabled = true;
    }
}