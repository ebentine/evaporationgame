﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawns atoms when triggered.
/// </summary>
public class SpawnAtomsPresentationItem : PresentationItem
{
    public int NumberOfSpawnTicks;
    public float SpawnTickRate;
    public int NumberToSpawnPerTick;
    private AtomCloudTimeIntegrator _integrator;
    private AtomCloud _spawner;

    public override void Start()
    {
        base.Start();
        _integrator = FindObjectOfType<AtomCloudTimeIntegrator>();
        if (_integrator == null)
            throw new Exception("Could not find integrator.");

        _spawner = FindObjectOfType<AtomCloud>();
        if (_spawner == null)
            throw new Exception("Could not find atom spawner.");
    }

    public override void Trigger()
    {
        base.Trigger();
        StartCoroutine(SpawnAtoms());
    }

    public IEnumerator SpawnAtoms()
    {
        for (int i = 0; i < NumberOfSpawnTicks; i++)
        {
            _spawner.Spawn(NumberToSpawnPerTick).ForEach(a => _integrator.AddAtom(a));
            yield return new WaitForSeconds(1f / SpawnTickRate);
        }
    }
}