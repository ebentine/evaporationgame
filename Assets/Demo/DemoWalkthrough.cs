﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoWalkthrough : MonoBehaviour {

    private AtomCloudTimeIntegrator _integrator;
    private AtomCloud _spawner;
    private TextMesh _textMesh;

    private MeshRenderer _origin;
    private MeshRenderer _chartMesh;
    private TemperatureChart _chart;

    private ManualRFKnife _knife;
    private MeshRenderer _knifeMesh;

	void Start () {
        
        _integrator = FindObjectOfType<AtomCloudTimeIntegrator>();
        if (_integrator == null) 
            throw new Exception("Could not find integrator.");

        _spawner = FindObjectOfType<AtomCloud>();
        if (_spawner == null)
            throw new Exception("Could not find atom spawner.");

        _textMesh = GetComponentInChildren<TextMesh>();
        if (_textMesh == null)
            throw new Exception("Could not find text mesh.");

        var origGO = transform.Find("Origin");
        if (origGO == null)
            throw new Exception("Could not find origin.");
        _origin = origGO.GetComponent<MeshRenderer>();
        if (_origin == null)
            throw new Exception("Could not find MeshRenderer on origin.");

        var chartGO = transform.Find("Chart");
        if (chartGO == null)
            throw new Exception("Could not find chart object.");
        _chartMesh = chartGO.GetComponent<MeshRenderer>();
        if (_chartMesh == null)
            throw new Exception("Could not find chart mesh renderer.");
        _chart = chartGO.GetComponent<TemperatureChart>();
        if (_chart == null)
            throw new Exception("Could not find chart.");

        _knife = GameObject.FindObjectOfType<ManualRFKnife>();
        if (_knife == null)
            throw new Exception("Could not find knife.");
        _knifeMesh = _knife.transform.GetComponent<MeshRenderer>();
        if (_knifeMesh == null)
            throw new Exception("Could not find knife mesh renderer.");
        _knife.enabled = false;
        _knifeMesh.enabled = false;

        StartCoroutine(IntroductionSequence());

	}

    #region Text Fading 

    /// <summary>
    /// Timer until text will fade.
    /// </summary>
    private float _timeToTextFade = 0.0f;

    /// <summary>
    /// Duration over which text fades.
    /// </summary>
    private float _textFadeDuration = 2.0f;

    private bool _textFading = false;

    public void TextFade(float timer)
    {
        _textFading = true;
        _timeToTextFade = timer;
    }

    public void ShowText(string text)
    {
        _textFading = false;
        _textMesh.text = text;
        _textMesh.color = Color.white;
    }

    public void TextFadeUpdate()
    {
        if (!_textFading)
            return;

        _timeToTextFade -= Time.deltaTime;
        var c = _textMesh.color;
        c.a = Mathf.Max(0f, Mathf.Min(1f, _timeToTextFade / _textFadeDuration));
        _textMesh.color = c;
        if (_timeToTextFade < 0)
            _textFading = false;
    }

    #endregion

    #region Springs

    /// <summary>
    /// Should we draw spring forces on objects?
    /// </summary>
    private bool _drawSprings = false;

    void SpringDrawUpdate()
    {
        if (!_drawSprings)
            return;

        _integrator.Atoms.ForEach(a => Debug.DrawLine(a.Position, Vector3.zero, Color.red, 0));
    }

    #endregion

    // Update is called once per frame
	void Update () {
        TextFadeUpdate();
        SpringDrawUpdate();
	}

    public IEnumerator IntroductionSequence()
    {
        // Hi!
        _textMesh.fontSize = 150;
        ShowText("Hi!");
        TextFade(7f);
        yield return new WaitForSeconds(8f);

        // This is an atom
        ShowText("This is an atom");
        _textMesh.fontSize = 80;
        yield return new WaitForSeconds(1f);
        _spawner.Spawn(1).ForEach((a) =>_integrator.AddAtom(a));
        TextFade(3f);
        yield return new WaitForSeconds(3f);

        // It is confined by a spring-like force
        ShowText("It orbits the origin,\n   confined by a spring-like force.");
        yield return new WaitForSeconds(1f);
        _origin.enabled = true;
        _drawSprings = true;

        yield return new WaitForSeconds(5f);
        TextFade(1f);
        _drawSprings = false;
        _origin.enabled = false;

        // This atom can be in one of two states
        ShowText("It can be in one of two states:");
        yield return new WaitForSeconds(4f);
        ShowText("It can be in one of two states:\n 1) where it is trapped");
        _origin.enabled = true;
        _drawSprings = true;
        yield return new WaitForSeconds(4f);
        _drawSprings = false;
        _origin.enabled = false;
        ShowText("It can be in one of two states:\n 1) where it is trapped\n 2) where it falls");
        _integrator.Atoms.ForEach(a => a.IsTrapped = false);
        yield return new WaitForSeconds(3f);
        TextFade(1f);
        yield return new WaitForSeconds(2f);

        yield return AtomInteractionsSequence();

    }

    public IEnumerator AtomInteractionsSequence()
    {
        yield return new WaitForSeconds(1f);
        _spawner.Spawn(15).ForEach(a => _integrator.AddAtom(a));
        ShowText("Pairs of atoms can collide,\n   exhanging energy and momentum.");
        yield return new WaitForSeconds(10f);
        TextFade(1f);

        yield return new WaitForSeconds(2f);
        ShowText("Let's add some more atoms...");
        yield return new WaitForSeconds(2f);
        for (int i = 0; i < 100; i++)
        {
            _spawner.Spawn(2).ForEach(a => _integrator.AddAtom(a));
            yield return new WaitForSeconds(3f/100);
        }
        TextFade(1f);
        yield return new WaitForSeconds(1f);
        
        ShowText("A cloud of atoms has a temperature, T.");
        yield return new WaitForSeconds(3f);
        TextFade(1f);
        yield return new WaitForSeconds(1f);
        ShowText("This is plotted on the graph below.");
        yield return new WaitForSeconds(1f);
        _chart.Clear();
        _chartMesh.enabled = true;
        yield return new WaitForSeconds(2f);
        TextFade(1f);
        yield return new WaitForSeconds(1f);

        // Make a cut
        yield return new WaitForSeconds(2f);
        ShowText("We remove atoms with greater than average energy.");
        yield return new WaitForSeconds(2f);
        _integrator.Atoms.ForEach(atom => atom.IsTrapped = atom.Position.sqrMagnitude < 1.2);
        TextFade(1f);
        yield return new WaitForSeconds(2f);
        ShowText("The remaining atoms have a lower average energy.");
        yield return new WaitForSeconds(3f);
        TextFade(1f);
        yield return new WaitForSeconds(1f);
        ShowText("The cloud has a lower temperature.");
        yield return new WaitForSeconds(3f);
        TextFade(1f);
        yield return new WaitForSeconds(4f);
        yield return CoolingStage();
    }

    /// <summary>
    /// The player cuts into a distribution.
    /// </summary>
    /// <returns></returns>
    public IEnumerator CoolingStage()
    {
        yield return new WaitForSeconds(0.5f);
        RefreshAtomCloud(0);
        ShowText("Let's add some more atoms.");
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < 100; i++)
        {
            _spawner.Spawn(5).ForEach(a => _integrator.AddAtom(a));
            yield return new WaitForSeconds(3f / 100);
        }
        TextFade(1f);
        yield return new WaitForSeconds(2f);

        
        ShowText("In our experiment,\n we remove hotter atoms using radiofrequency waves.");
        yield return new WaitForSeconds(3f);
        _knife.enabled = true;
        _knife.PlayerControlled = false;
        _knifeMesh.enabled = true;

        yield return new WaitForSeconds(1f);
        TextFade(1f);
        yield return new WaitForSeconds(1f);

        ShowText("This removes atoms that touch the grey sphere.");
        yield return new WaitForSeconds(4f);
        TextFade(1f);
        yield return new WaitForSeconds(1f);

        ShowText("It's radius selects the potential energy at which to remove atoms.");
        yield return new WaitForSeconds(4f);
        TextFade(1f);
        yield return new WaitForSeconds(1f);

        ShowText("Decreasing the radius removes atoms at a lower energy.");
        yield return new WaitForSeconds(4f);
        TextFade(1f);
        yield return new WaitForSeconds(1f);

    }

    public void RefreshAtomCloud(int number)
    {
        List<IAtom> atoms = new List<IAtom>(_integrator.Atoms);
        atoms.ForEach(a => _integrator.RemoveAtom(a));
        atoms.ForEach(a => a.Destroy());
        _spawner.Atoms.Clear();
        _spawner.Spawn(number).ForEach(a => _integrator.AddAtom(a));
    }

}
