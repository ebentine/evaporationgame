﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

/// <summary>
/// An item in a presentation is shown on a specified Cue.
/// </summary>
public class PresentationItem : MonoBehaviour {

    /// <summary>
    /// The cue that triggers this item.
    /// </summary>
    public string TriggerCue = "";

    /// <summary>
    /// Duration until next cue is triggered.
    /// </summary>
    public float NextTriggerDuration = 0f;

    /// <summary>
    /// Next cue to trigger.
    /// </summary>
    public string NextCue = "";

    private PresentationMgr _cueManager;

	// Use this for initialization
	public virtual void Start () {

        _cueManager = GameObject.FindObjectOfType<PresentationMgr>();
        if (_cueManager == null)
            throw new Exception("Could not find manager.");

        _cueManager.Register(this);
	}
	
	// Update is called once per frame
	public virtual void Update () {
		
	}

    public virtual void Trigger()
    {
        StartCoroutine(TriggerNext());
    }

    public IEnumerator TriggerNext()
    {
        yield return new WaitForSeconds(NextTriggerDuration);
        if (NextCue != "")
            _cueManager.Trigger(NextCue);
    }
}
