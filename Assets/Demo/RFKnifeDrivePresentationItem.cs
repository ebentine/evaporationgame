﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Drops atoms when triggered.
/// </summary>
public class RFKnifeDrivePresentationItem : PresentationItem
{
    private ManualRFKnife _RFKnife;

    public override void Start()
    {
        base.Start();
        _RFKnife = FindObjectOfType<ManualRFKnife>();
        if (_RFKnife == null)
            throw new Exception("Could not find integrator.");
    }

    public override void Trigger()
    {
        base.Trigger();
        _running = true;
        _RFKnife.CutRate = CutRate;
        _timer = CutFor;
    }

    private float _timer;
    private bool _running;
    public float CutRate = -0.1f;
    public float CutFor = 1f;

    public override void Update()
    {
        base.Update();
        
        if (!_running) return;
        _timer -= Time.deltaTime;
        if (_timer < 0f)
        {
            _running = false;
            _RFKnife.CutRate = 0f;
        }

    }
}