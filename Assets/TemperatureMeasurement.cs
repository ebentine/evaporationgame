﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemperatureMeasurement : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        //locate IPotentials
    }

    public void Reset()
    {
        Renderer render = GetComponent<Renderer>();
        if (render != null)
            render.material.color = Color.white;
    }

    public int MinCount = 10;
    public float Temperature;
    float lastUpdate = -100f;

    // Update is called once per frame
    void Update()
    {
        if (Time.time < lastUpdate + 0.5f)
            return;

        lastUpdate = Time.time;

        //Get all atoms
        int count = 0;
        float energy = 0.0f;
        foreach (AtomBehaviour a in FindObjectsOfType<AtomBehaviour>())
        {
            if (!a.IsTrapped)
                continue;
            energy += a.Velocity.sqrMagnitude;
            count++;
        }

        if (count > MinCount)
            Temperature = energy / count;

        TextMesh tm = GetComponent<TextMesh>();
        if (tm != null)
        {

            tm.text = "Temperature: " + Temperature.ToString("000.0");
            if (count < MinCount)
            {
                Renderer render = GetComponent<Renderer>();
                if (render != null)
                    render.material.color = Color.gray;
            }
        }
    }
}

